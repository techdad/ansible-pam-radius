Ansible Role: PAM Radius Authentication
=========

Role Variables
--------------

```
vars:
    radius_servers:
      - rad_server: "172.17.30.4"
        rad_secret: "azerty"
      - rad_server: "192.168.10.6"
        rad_secret: "qwerty"
  ```

License
-------

Apache 2.0
